# Frontend Mentor - Stats preview card component solution

This is a solution to the [Stats preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/stats-preview-card-component-8JqbgoU62). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - Stats preview card component solution](#frontend-mentor---stats-preview-card-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [Useful resources](#useful-resources)
  - [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size

### Screenshot

![Screenshot](./screenshot.png)

### Links

- Solution URL: [https://github.com/nemanjau/stats-preview-card-component](https://github.com/nemanjau/stats-preview-card-component)
- Live Site URL: [https://nemanjau.github.io/stats-preview-card-component/](https://nemanjau.github.io/stats-preview-card-component/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow

### Useful resources

- [MDN Web Docs](https://developer.mozilla.org/en-US/) - This helped me with responsive images and blend mode.
- [CSS-Tricks](https://css-tricks.com/) - This helped me with there amazing Flexbox and CSS Grid Guide.

## Author

- Website - [Nemanja Unković](https://github.com/nemanjau/)

